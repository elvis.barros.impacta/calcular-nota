from flask import Flask, render_template, request
import json

app = Flask(__name__)

@app.route("/nota")
def main():
    nota1 = request.args.get("primeira")
    nota2 = request.args.get("segunda")

    media = None
    resultado = None

    if nota1 and nota2:
        nota1 = float(nota1)
        nota2 = float(nota2)

        media = (nota1 + nota2) / 2

        if media >= 7:
            resultado = 'Aprovado'
        elif media >= 4:
            resultado = 'Recuperação'
        else:
            resultado = 'Reprovado'
    return render_template('index.html', media=media, resultado=resultado)


if __name__ == '__main__':
    app.run()